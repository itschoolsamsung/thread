package com.example.thread;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*
        * при нажатии на первую кнопку запускаем цикл из 10 итераций в основном потоке -
        * приложение блокируется
         */
        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0; i<10; i++) {
                    try {
                        Thread.sleep(1000); //задержка на 1 секунду - блокирует текущий поток
                    } catch (InterruptedException e) {
                    }
                    Log.i("no-thread", String.valueOf(i));
                }
            }
        });

        /*
         * при нажатии на вторую кнопку запускаем отдельный поток с циклом из 10 итераций -
         * приложение не блокируется
         */
        findViewById(R.id.button2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyThread t = new MyThread();
                t.start();
            }
        });
    }

    class MyThread extends Thread {
        @Override
        public void run() {
            for (int i=0; i<10; i++) {
                try {
                    Thread.sleep(1000); //задержка на 1 секунду - блокирует текущий поток
                } catch (InterruptedException e) {
                }
                Log.i("thread", String.valueOf(i));
            }
        }
    }
}
